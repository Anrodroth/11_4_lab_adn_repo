﻿namespace Test_Driven_Development
{
    using System;

    /// <summary>
    /// A class used to represent employees.
    /// </summary>
    [Serializable]
    public class Employee
    {
        /// <summary>
        /// The hourly wage of the employee.
        /// </summary>
        private decimal hourlyWage;

        /// <summary>
        /// The hours scheduled for the employee.
        /// </summary>
        private int hoursScheduled;

        /// <summary>
        /// Initializes a new instance of the Employee class.
        /// </summary>
        /// <param name="hourlyWage">The hourly wage of the employee.</param>
        /// <param name="hoursScheduled">The hours scheduled for the employee.</param>
        public Employee(decimal hourlyWage, int hoursScheduled)
        {
            this.hourlyWage = hourlyWage;
            this.hoursScheduled = hoursScheduled;
        }

        /// <summary>
        /// Prevents a default instance of the Employee class from being created.
        /// </summary>
        private Employee()
        {
        }

        /// <summary>
        /// Gets the hours scheduled for the employee.
        /// </summary>
        public int HoursScheduled { get => this.hoursScheduled; }

        /// <summary>
        /// Gets or sets the paycheck.
        /// </summary>
        public decimal Paycheck { get; set; }

        /// <summary>
        /// A method that calculates how much work the employee will do and has left to do.
        /// </summary>
        /// <param name="work">The work that needs to be done.</param>
        public void DoWork(Job work)
        {
            //// If the hoursScheduled are less than the time remaining on the job, 
            //// reduce the hours remaining on the job by the hours scheduled, 
            //// and set hours scheduled to zero.
            //// Else reduce hours scheduled by time remaining, and set time investment for job to 0.

            if (this.hoursScheduled < work.TimeInvestmentRemaining && this.hoursScheduled > 0)
            {
                this.Paycheck = this.hoursScheduled * this.hourlyWage;
                work.TimeInvestmentRemaining -= this.hoursScheduled;
            }
            else if (this.hoursScheduled == work.TimeInvestmentRemaining)
            {
                this.Paycheck = this.hoursScheduled * this.hourlyWage;
                this.hoursScheduled = 0;
                work.TimeInvestmentRemaining = 0;
            }
            else
            {
                this.Paycheck = this.hoursScheduled * this.hourlyWage;
                this.hoursScheduled -= work.TimeInvestmentRemaining;
                work.TimeInvestmentRemaining = 0;
            }

            work.JobCost = this.Paycheck * 1.5m;
        }
    }
}
