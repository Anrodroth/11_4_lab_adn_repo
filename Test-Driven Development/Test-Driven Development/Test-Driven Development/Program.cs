﻿namespace Test_Driven_Development
{
    using System.IO;
    using System.Xml.Serialization;

    /// <summary>
    /// A class used to represent a program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// The main method.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            // Create the business.
            Business business = new Business("Genericorp.");

            // Add employees.
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Add a job to the workflow.
            business.AddJob(new Job(15));

            // Let the business do it's work.
            business.DoWork();

            // Serialize the business to an XML file.
            SerializeBusiness(business);
        }

        /// <summary>
        /// A method that serializes a business into an XML file. If the file does not exist, one is generated automatically.
        /// </summary>
        /// <param name="business">The business to be serialized.</param>
        private static void SerializeBusiness(Business business)
        {
            // Instantiate the serializer.
            XmlSerializer serializer = new XmlSerializer(typeof(Business));

            // Get the path.
            string xmlPath = @"C:\ADN\testDoc.xml";

            // If the XML file does not exist, attempt to deserialize the XML file if it exists.
            try
            {
                // Deserialize the XML file if it already exists.
                using (var streamReadVar = new StreamReader(xmlPath))
                {
                    business = (Business)serializer.Deserialize(streamReadVar);
                }
            }
            catch (System.IO.IOException)
            {
                // Else, catch the System.IO.IOException and create the XML file.
                using (Stream stream = File.Create(xmlPath))
                {
                    serializer.Serialize(stream, business);
                }
            }
        }
    }
}
