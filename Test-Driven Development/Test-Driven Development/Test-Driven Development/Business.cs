﻿namespace Test_Driven_Development
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class used to represent a business. 
    /// </summary>
    [Serializable]
    public class Business
    {
        /// <summary>
        /// Initializes a new instance of the Business class.
        /// </summary>
        /// <param name="businessName">The name of the business.</param>
        public Business(string businessName)
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
            this.BusinessName = businessName;
        }

        /// <summary>
        /// Prevents a default instance of the Business class from being created.
        /// </summary>
        private Business()
        {
        }

        /// <summary>
        /// Gets or sets a list of employees.
        /// </summary>
        public List<Employee> Employees { get; set; }

        /// <summary>
        /// Gets or sets a list of jobs.
        /// </summary>
        public List<Job> Jobs { get; set; }

        /// <summary>
        /// Gets or sets the name of the business.
        /// </summary>
        public string BusinessName { get; set; }

        /// <summary>
        /// A method used to add an employee.
        /// </summary>
        /// <param name="employee">The employee being added.</param>
        public void AddEmployee(Employee employee)
        {
            // Add employee to the employee list.
            this.Employees.Add(employee);
        }

        /// <summary>
        /// Adds a job to the business.
        /// </summary>
        /// <param name="job">The job being added.</param>
        public void AddJob(Job job)
        {
            // Add job to the job list
            this.Jobs.Add(job);
        }

        /// <summary>
        /// A method used to do work for the business.
        /// </summary>
        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees
            // to see if they have hours and have them DoWork(). 
            // If job is completed after work, break from loop.

            // For each job (x)...
            this.Jobs.ForEach(x => 
            {
                // If job (x) is not completed, loop through the employees.
                if (!x.JobCompleted)
                {
                    foreach (Employee e in this.Employees)
                    {
                        if (e.HoursScheduled > 0 && !x.JobCompleted)
                        {
                            e.DoWork(x);
                        }
                        else if (x.JobCompleted)
                        {
                            break;
                        }
                    }
                }
            });
        }
    }
}
