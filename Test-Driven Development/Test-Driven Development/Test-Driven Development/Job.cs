﻿namespace Test_Driven_Development
{
    using System;

    /// <summary>
    /// A class used to represent a job.
    /// </summary>
    [Serializable]
    public class Job
    {
        /// <summary>
        /// Initializes a new instance of the Job class.
        /// </summary>
        /// <param name="timeInvestment">The time investment for the job.</param>
        public Job(int timeInvestment)
        {
            this.TimeInvestmentRemaining = timeInvestment;
        }

        /// <summary>
        /// Prevents a default instance of the Job class from being created.
        /// </summary>
        private Job()
        {
        }

        /// <summary>
        /// Gets or sets the cost of the job.
        /// </summary>
        public decimal JobCost { get; set; }

        /// <summary>
        /// Gets or sets the time investment remaining for the job.
        /// </summary>
        public int TimeInvestmentRemaining { get; set; }

        /// <summary>
        /// Gets a value indicating whether or not the job was completed.
        /// </summary>
        public bool JobCompleted => this.TimeInvestmentRemaining == 0;
    }
}
