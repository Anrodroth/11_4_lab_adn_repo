namespace BusinessTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Test_Driven_Development;

    /// <summary>
    /// A unit test class used to house the unit tests for the business methods.
    /// </summary>
    [TestClass]
    public class BusinessTests
    {
        /// <summary>
        /// A test to check if an employee is successfully added to the list.
        /// </summary>
        [TestMethod]
        public void EmployeeSuccessfullyAddedToList()
        {
            // Arrange
            Business business = new Business("A business");

            // Act
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));

            // Assert
            Assert.AreEqual(2, business.Employees.Count);
        }

        /// <summary>
        /// A test method used to check if a job was successfully added to the list.
        /// </summary>
        [TestMethod]
        public void JobAddedToListSuccessfullyTest()
        {
            // Arrange
            Business business = new Business("A business");

            // Act
            business.AddJob(new Job(5));
            business.AddJob(new Job(10));

            // Assert
            Assert.AreEqual(2, business.Jobs.Count);
        }

        /// <summary>
        /// A method used to test if a job is actually complete after the work is done.
        /// </summary>
        [TestMethod]
        public void JobCompleteAfterWorkTest()
        {
            // Arrange
            Business business = new Business("A business");
            business.AddJob(new Job(2));
            business.AddEmployee(new Employee(10, 5));

            // Act
            business.DoWork();

            // Assert
            Assert.IsTrue(business.Jobs[0].JobCompleted);
        }

        /// <summary>
        /// A test method used to check if a job is incomplete when out of employees.
        /// </summary>
        [TestMethod]
        public void JobIncompleteWhenOutOfEmployees()
        {
            // Arrange
            Business business = new Business("A business");
            business.AddJob(new Job(15));
            business.AddEmployee(new Employee(10, 5));
            business.AddEmployee(new Employee(10, 7));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[0].JobCompleted);
        }

        /// <summary>
        /// A test method used to check if the second job is incomplete when employee hours match the first job.
        /// </summary>
        [TestMethod]
        public void SecondJobIsIncompleteWhenEmployeeHoursMatchFirstJob()
        {
            // Arrange
                //  bool hoursOnlyForFirstJob = true;
            Business business = new Business("A business");

                // 1st JOB Time Investment = 15
            business.AddJob(new Job(15));

                // 2nd JOB time Investment = 20
            business.AddJob(new Job(20));

                // Employee, wage = 10, Hours Scheduled = 10.
            business.AddEmployee(new Employee(10, 10));

            // Act
            business.DoWork();

            // Assert
            Assert.IsFalse(business.Jobs[1].JobCompleted);
        }

        /// <summary>
        /// A test method used to check if an employee payment was added correctly.
        /// </summary>
        [TestMethod]
        public void EmployeePaymentAddedCorrectly()
        {
            // Arrange
            Employee employee = new Employee(10, 10);

            // Act
            employee.DoWork(new Job(15));

            // Assert
            Assert.AreEqual(100, employee.Paycheck);
        }

        /// <summary>
        /// A test method used to check if the job cost is added correctly once all the work is done.
        /// </summary>
        [TestMethod]
        public void IsJobCostAddedCorrectlyWhenWorkIsDone()
        {
            // Arrange
            Business business = new Business("A business");
            business.AddJob(new Job(10));
            business.AddEmployee(new Employee(10, 10));

            // Act
            business.DoWork();

            // Assert
            Assert.AreEqual(business.Employees[0].Paycheck * 1.5m, business.Jobs[0].JobCost);
        }
    }
}
